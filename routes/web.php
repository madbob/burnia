<?php

Route::get('/', 'CommonController@home')->name('common.home');
Route::get('test', 'CommonController@test')->name('common.test');

Route::get('initial_token', 'TokenController@initial')->name('token.initial');
Route::get('check/{token?}', 'TokenController@check')->name('token.check');
