# Burnia

Burnia is a simple script which permits you to easily identify spam bots, harvesters and other malicious agents crawling the web.

Contrary to the classic captcha, difficult and annoying to understand, Burnia evaluates the IP address of the user who wants to fill out your form and blocks any harmful sources: it is built on top of [Project Honeypot](https://www.projecthoneypot.org), a global effort to monitor and index toxic sources on the Internet.

A public instance is available at http://burnia.madbob.org/

## Installation

```
git clone https://gitlab.com/madbob/burnia.git
cd burnia
composer install
cp .env.example .env
(edit .env with your parameters, in particular the Project Honeypot API key)
php artisan key:generate
```

You need to [create an account on Project Honeypot](https://www.projecthoneypot.org/create_account.php) to obtain an API key. It would be also appreciated to host an honeypot on your website: read [this page](https://www.projecthoneypot.org/manage_honey_pots.php) and replace the `public/territorywicket.php` file accordly.

## License

Burnia is distributed under the terms of AGPLv3 license.

Copyright 2017 Roberto Guido <bob@linux.it>
