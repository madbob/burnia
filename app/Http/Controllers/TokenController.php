<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Log;
use Cache;

use Thasmo\ProjectHoneypot\Blacklist;
use Ramsey\Uuid\Uuid;

class TokenController extends Controller
{
    public function initial(Request $request)
    {
        $ip_address = $request->ip();
        $client = new Blacklist($ip_address, env('PROJECTHONEYPOT_KEY'));

        if ($client->isListed()) {
            Log::info('Bad guy detected: ' . $ip_address);
            abort(403);
        }
        else {
            try {
                while(true) {
                    $uuid = Uuid::uuid4();
                    if (Cache::has($uuid))
                        continue;

                    Cache::put($uuid, 'true', 10);
                    echo $uuid;
                    break;
                }
            }
            catch(\Exception $e) {
                Log::error('Unable to dispatch valid token: ' . $e->getMessage());
                abort(403);
            }
        }
    }

    public function check(Request $request, $token = null)
    {
        if (Cache::has($token)) {
            Cache::forget($token);
            return 'ok';
        }
        else {
            Log::info('Bad token detected: ' . $uuid);
            echo 'ko';
            abort(403);
        }
    }
}
