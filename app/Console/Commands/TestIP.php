<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Thasmo\ProjectHoneypot\Blacklist;

class TestIP extends Command
{
    protected $signature = 'test {ip}';
    protected $description = 'Handy command to directly test an IP';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $ip = $this->argument('ip');

        $client = new Blacklist($ip, env('PROJECTHONEYPOT_KEY'));
        $client->query();

        echo "Search Engine: " . ($client->isSearchEngine() ? 'YES' : 'NO') . "\n";
        echo "Suspicious: " . ($client->isSuspicious() ? 'YES' : 'NO') . "\n";
        echo "Harvester: " . ($client->isHarvester() ? 'YES' : 'NO') . "\n";
        echo "Spammer: " . ($client->isSpammer() ? 'YES' : 'NO') . "\n";
        echo "Listed: " . ($client->isListed() ? 'YES' : 'NO') . "\n";

        print_r($client->getResult());
    }
}
