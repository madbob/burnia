function initBurnia() {
    var widget = document.querySelector('#burnia');
    widget.innerHTML = '';

    var div = document.createElement('div');
    div.classList.add('burnia-widget');

    var css = document.createElement('link');
    css.rel = 'stylesheet';
    css.href = '/css/burnia.css';
    div.appendChild(css);

    var token = document.createElement('input');
    token.type = 'hidden';
    token.name = 'burnia-token';
    token.value = 'broken';
    div.appendChild(token);

    var checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.required = 'required';
    checkbox.addEventListener('click', function(e) {
        checkbox.disabled = 'disabled';

        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/initial_token');
        xhr.onload = function() {
            var value = '';
            var label = document.createElement('span');

            if (xhr.status === 200) {
                value = xhr.responseText;
                label.innerHTML = '&#10003;';
                label.classList.add('burnia-correct');
            }
            else {
                value = 'broken';
                label.innerHTML = '&#10060;';
                label.classList.add('burnia-broken');
            }

            checkbox.parentNode.replaceChild(label, checkbox);
            token.value = value;

            /*
                After 10 minutes, token expires
            */
            setTimeout(function() {
                initBurnia();
            }, 10 * 60 * 1000);
        };
        xhr.send();
    });
    div.appendChild(checkbox);

    var label = document.createElement('label');
    label.innerHTML = 'I\'m not a robot';
    div.appendChild(label);

    widget.appendChild(div);
}

initBurnia();
