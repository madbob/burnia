<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Burnia</title>

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/mine.css">

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <meta property="og:site_name" content="Burnia" />
        <meta property="og:title" content="Burnia" />
        <meta property="og:url" content="http://burnia.madbob.org/" />
        <meta property="og:type" content="website" />
        <meta property="og:locale" content="it_IT" />
    </head>
    <body>
        <div>
            <section class="header-wrapper text-center">
                <div>
                    <img class="img-fluid" src="/images/logo.png">
                    <h3>A captcha,<br>without captcha</h3>
                </div>
            </section>

            <section class="features-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="card-deck">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title text-center"><i class="fa fa-check-square-o fa-3x" aria-hidden="true"></i></h4>
                                        <p class="card-text">Keep away spam bots from your forms, without bothering your users.</p>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title text-center"><i class="fa fa-plug fa-3x" aria-hidden="true"></i></h4>
                                        <p class="card-text">A few lines of code, client and server side, and you are ready to go.</p>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title text-center"><i class="fa fa-server fa-3x" aria-hidden="true"></i></h4>
                                        <p class="card-text">Use this public instance for free, or host your own Burnia.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- FAQ -->

            <section class="questions-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col lead">
                            <h3>What is Burnia?</h3>
                            <p>
                                <strong>Burnia</strong> is a simple script which permits you to easily identify spam bots, harvesters and other malicious agents crawling the web.
                            </p>
                            <p>
                                Contrary to the classic captcha, difficult and annoying to understand, <strong>Burnia</strong> evaluates the IP address of the user who wants to fill out your form and blocks any harmful sources: it is built on top of <a href="https://www.projecthoneypot.org/">Project Honeypot</a>, a global effort to monitor and index toxic sources on the Internet.
                            </p>

                            <br>

                            <div class="mx-auto my-3 py-3 burnia-example">
                                <div class="text-center">
                                    <h3>As easy as this!</h3>
                                    <div id="burnia" class="mx-auto"></div>
                                    <script src="{{ asset('js/burnia.js') }}"></script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="usage-wrapper" id="usage-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h3>How to use</h3>
                            <br>

                            <div class="row">
                                <div class="col">
                                    <h5>Client side</h5>

                                    <p>
                                        In your form, add the row<br>
                                        <code>&lt;div id="burnia"&gt;&lt;/div&gt;</code><br>
                                        and, at the beginning or at the end of the document, include the script<br>
                                        <code>&lt;script src="{{ asset('js/burnia.js') }}"&gt;&lt;/script&gt;</code>
                                    </p>
                                </div>
                                <div class="col">
                                    <h5>Server side</h5>

                                    <p>
                                        When the form is submitted, get the <code>burnia-token</code> parameter.
                                    </p>
                                    <p>
                                        If it is missing or equal to <code>"broken"</code>, do not accept the input.
                                    </p>
                                    <p>
                                        Otherwise, call the endpoint<br>
                                        <code>{{ route('token.check', 'your-burnia-token') }}</code><br>
                                        to ask for confirmation.
                                    </p>
                                    <hr>
                                    <pre>if (isset($_POST['burnia-token']) == false)
    die();

$burnia = $_POST['burnia-token'];
if ($burnia == 'broken')
    die();

$url = sprintf('{{ route('token.check', '') }}/%s', $burnia);
$confirmation = file_get_contents($url);
if ($confirmation != 'ok')
    die();</pre>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="contacts-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h3>BURNIA</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col links">
                            <div class="row">
                                <div class="col-1">
                                    <p><i class="fa fa-envelope" aria-hidden="true"></i></p>
                                </div>
                                <div class="col-11">
                                    <p><a href="info@madbob.org">info@madbob.org</a></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-1">
                                    <p><i class="fa fa-gitlab" aria-hidden="true"></i></p>
                                </div>
                                <div class="col-11">
                                    <p><a href="https://gitlab.com/madbob/burnia">gitlab.com/madbob/burnia</a></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-1">
                                    <p><i class="fa fa-cogs" aria-hidden="true"></i></p>
                                </div>
                                <div class="col-11">
                                    <p>powered by <a href="http://madbob.org/"><img src="images/mad.png"></a></p>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row">
                                <div class="col-1">
                                    <p><i class="fa fa-eye" aria-hidden="true"></i></p>
                                </div>
                                <div class="col-11">
                                    <p><a href="/territorywicket.php">Don't Click Me</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <p>Support this project with a PayPal donation</p>
                            <a href="https://www.paypal.me/m4db0b">
                                <img src="/images/paypal.png">
                            </a>
                            <p>or a subscription on Patreon</p>
                            <a href="https://www.patreon.com/madbob">
                                <img src="/images/become_a_patron_button.png">
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <!-- Piwik -->
        <script type="text/javascript">
        var _paq = _paq || [];
        _paq.push(["setDoNotTrack", true]);
        _paq.push(["disableCookies"]);
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function() {
            var u="//stats.madbob.org/";
            _paq.push(['setTrackerUrl', u+'piwik.php']);
            _paq.push(['setSiteId', '10']);
            var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
            g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
        })();
        </script>
        <!-- End Piwik Code -->
    </body>
</html>
